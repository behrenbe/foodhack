//
//  Addresturant.swift
//  FoodHack
//
//  Created by  huangzhen on 11/11/2017.
//  Copyright © 2017 Blake Ehrenbeck. All rights reserved.
//

import UIKit
import FirebaseCore
import FirebaseDatabase
import FirebaseAuth
class Addresturant: UIViewController {

    var delegate:AddFriendViewController?
    var matchID:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    @IBOutlet weak var textfield: UITextField!
    
    
    @IBAction func searchbutton(_ sender: Any) {
        
        var ref:DatabaseReference!
        ref=Database.database().reference()
        ref.child("Restraunt").queryOrderedByKey().queryEqual(toValue: textfield.text).observe(.value, with: {(DataSnapshot) in
            
            if let snapDict = DataSnapshot.value as? [String:AnyObject] {
                
                for child in snapDict{
                    
                    let addresturant = self.storyboard?.instantiateViewController(withIdentifier: "outcome") as? UINavigationController
                    (addresturant!.viewControllers[0] as! TisprCardStackDemoViewController).restaurantDict = child
                    (addresturant!.viewControllers[0] as! TisprCardStackDemoViewController).matchID = self.matchID
                    self.present(addresturant!, animated: true, completion: nil)
                    
                    
                    
                }
                
            }
            
         
            
        })
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
