//
//  TisprCardStackDemoViewController.swift
//  FoodHack
//
//  Created by  huangzhen on 11/11/2017.
//  Copyright © 2017 Blake Ehrenbeck. All rights reserved.
//

import TisprCardStack
import UIKit
import FirebaseCore
import FirebaseAuth
import FirebaseDatabase

class TisprCardStackDemoViewController: TisprCardStackViewController, TisprCardStackViewControllerDelegate {
    
    var restaurantDict: (key:String, value:AnyObject)?
    var liked:[String]?
    var restaurants:[String]?
    var matchID:Int?
    
    
    fileprivate let colors = [
                              UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0),
                              UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0),
                              UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0),
                              UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0),
                              UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0),
                              UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    ]
    
    
    fileprivate var countOfCards: Int = 0
    
    @IBAction func done(_ sender: Any) {
        print(restaurants)
        let mid = matchID!
        print(mid)
        
        var ref: DatabaseReference!
        
        ref = Database.database().reference()
        
        ref.child("Match/\(mid)/\((Auth.auth().currentUser?.uid)!)").setValue(["liked":restaurants!])
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
  
        liked = []
        restaurants = []
        
        for key in (((((restaurantDict?.value as? NSDictionary)?.value(forKey: "Menu")) as? NSDictionary))?.allKeys)!{
            
            let keyString = key as! String
            
            print((((((restaurantDict?.value as? NSDictionary)?.value(forKey: "Menu")) as? NSDictionary))?.value(forKey: keyString) as? NSDictionary)?.value(forKey: "Picture"))
         
            //addNewCards(self)
            
            countOfCards += 1
        }
        
        //set animation speed works?
        setAnimationSpeed(0.85)
        
        //set size of cards
        let size = CGSize(width: view.bounds.width - 40, height: 2*view.bounds.height/3)
        setCardSize(size)
        
        cardStackDelegate = self
        
        //configuration of stacks
        layout.topStackMaximumSize = 4
        layout.bottomStackMaximumSize = 30
        layout.bottomStackCardHeight = 45
    }
    
    //method to add new card

    
    override func numberOfCards() -> Int {
        return countOfCards
    }
    
    override func card(_ collectionView: UICollectionView, cardForItemAtIndexPath indexPath: IndexPath) -> TisprCardStackViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "democell", for: indexPath as IndexPath) as! TisprCardStackDemoViewCell
        
        cell.delegate = self
        
        let key = (((((restaurantDict?.value as? NSDictionary)?.value(forKey: "Menu")) as? NSDictionary))?.allKeys)![indexPath.item]
        
        let keyString = key as! String
        
        
        let url = URL(string:   (((((restaurantDict?.value as? NSDictionary)?.value(forKey: "Menu")) as? NSDictionary))?.value(forKey: keyString) as? NSDictionary)?.value(forKey: "Picture") as! String)
        
        let menuItem = ((((restaurantDict?.value as? NSDictionary)?.value(forKey: "Menu")) as? NSDictionary))?.allKeys[indexPath.item]
        
        let pricetag = (((((restaurantDict?.value as? NSDictionary)?.value(forKey: "Menu")) as? NSDictionary))?.value(forKey: keyString) as? NSDictionary)?.value(forKey: "Price")
        
        let menuItemString = menuItem as! String
        
        let menuPrice = pricetag as! Double
        
        let post = String(menuPrice)
        
        
        cell.Price.text = "$"+post
        
        print(menuItemString)
        
        cell.Rname.text=menuItemString
        
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width:0, height:1)
        cell.layer.shadowOpacity = 1
        cell.layer.shadowRadius = 1.0
        cell.clipsToBounds = true
        
        
        do {
            
            if let url = url{
                let data = try Data(contentsOf: url)
                cell.Rimage.image = UIImage(data: data)
            }
            
        }catch let error {
            print("pls no")
        }
      
        
        
    
        
        cell.contentView.backgroundColor = colors[indexPath.item % colors.count]
        
        return cell
        
    }
    

    
    func cardDidChangeState(_ cardIndex: Int) {
        
        print("card with index - \(cardIndex) changed position")
    }
}
