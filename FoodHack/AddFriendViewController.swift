//
//  AddFriendViewController.swift
//  FoodHack
//
//  Created by Blake Ehrenbeck on 11/11/17.
//  Copyright © 2017 Blake Ehrenbeck. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseCore
import FirebaseAuth

class AddFriendViewController: UIViewController {

    var specialKey:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var userField: UITextField!
    
    @IBAction func goforward(_ sender: Any) {
    
        let addfriendsStoryboard = self.storyboard?.instantiateViewController(withIdentifier: "addrestaurant") as! Addresturant
        addfriendsStoryboard.delegate = self
        addfriendsStoryboard.matchID = specialKey!
            self.present(addfriendsStoryboard, animated: true, completion: nil)
     
        
    }
    @IBAction func addUser(_ sender: Any) {
    
        var ref: DatabaseReference!
        
        ref = Database.database().reference()
        
        
        
        
        
        ref.child("users").queryOrdered(byChild: "displayName").queryEqual(toValue: userField.text).observeSingleEvent(of:.value, with:{(DataSnapshot) in
            
            if let snapDict = DataSnapshot.value as? [String:AnyObject]{
               
                
                print(snapDict)
                
                for child in snapDict{
                    
                    if let name = child.value["displayName"] as? String{
                       
                        self.specialKey = Int(arc4random())
                        ref.child("users").child((Auth.auth().currentUser?.uid)!).updateChildValues(["Friend":child.key, "MatchID":self.specialKey!])
                        
                        ref.child("users").child(child.key).updateChildValues(["Friend":Auth.auth().currentUser?.uid, "MatchID":self.specialKey!])
                        
                        ref.child("Match").child("\(self.specialKey!)").setValue(["Users":[child.key, Auth.auth().currentUser?.uid]])
                        
                        
                        
                        
                    }
                  
                }
            
        }
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
