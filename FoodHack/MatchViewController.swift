//
//  MatchViewController.swift
//  FoodHack
//
//  Created by Blake Ehrenbeck on 11/12/17.
//  Copyright © 2017 Blake Ehrenbeck. All rights reserved.
//

import UIKit
import FirebaseCore
import FirebaseAuth
import FirebaseDatabase

class MatchViewController: UIViewController {

    var ref: DatabaseReference!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ref = Database.database().reference()
        
        ref.child("users/\((Auth.auth().currentUser?.uid)!)/MatchID").observeSingleEvent(of: .value, with: {(MyDataSnapshot) in
            
            
            print("Hopefully \(MyDataSnapshot.value)")
            
            self.ref.child("Match/\((MyDataSnapshot.value as! NSNumber).stringValue)").queryOrderedByKey().observeSingleEvent(of: .value, with: {(DataSnapshot) in
                
                
                
                
                
            })
        })
        
        
      
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
