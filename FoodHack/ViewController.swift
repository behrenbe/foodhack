//
//  ViewController.swift
//  FoodHack
//
//  Created by Blake Ehrenbeck on 11/11/17.
//  Copyright © 2017 Blake Ehrenbeck. All rights reserved.
//

import UIKit
import FirebaseCore
import FirebaseAuth
import FirebaseDatabase
import GoogleSignIn

class ViewController: UIViewController, GIDSignInUIDelegate, GIDSignInDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self

        
        if Auth.auth().currentUser != nil {
            print("you're in")
        } else {
            //User Not logged in
            print("you're not in")
        }
        
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!)  {
        // ...
       
        print("signing in")
        
        if let error = error {
            // ...
            print("Error is \(error)")
            return
        }
        
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        // ...
        
        Auth.auth().signIn(with: credential) { (user, error) in
            
            print("got here")
            
            if let error = error {
                // ...
                print("Error is \(error)")
                return
            }
            // User is signed in
            // ...
            print("you're in")
            
            
            var ref: DatabaseReference!
            
            ref = Database.database().reference()
            
            ref.child("users").child((user?.uid)!).setValue(["displayName":user?.displayName])
            
            
            let addfriendsStoryboard = self.storyboard?.instantiateViewController(withIdentifier: "AddFriends")
            if let storyboard = addfriendsStoryboard{
                self.present(storyboard, animated: true, completion: nil)
            }else{
            }
            
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

